## 使用Pip TensorFlow 安装流程
   

**参考资料** 
> [TensorFlow官网-使用Pip TensorFlow 安装流程](https://www.tensorflow.org/install/pip )
#### 环境要求
- Ubuntu 16.04 or later (64-bit)
- macOS 10.12.6 (Sierra) or later (64-bit) (no GPU support)
- Windows 7 or later (64-bit) (Python 3 only)
- Raspbian 9.0 or later
#### 概念说明



#### 安装步骤（Windows 10）
1. 安装python开发环境（python3）

```
python3 --version
pip3 --version
virtualenv --version
```   
如果已安装，则检查python安装版本        
``` 
python --version 
```       
输出内容：
  ```
  Python 3.6.4
  ```
选择pip作为python组件

```
pip3 install -U pip virtualenv
```

2. 创建虚拟环境

python虚拟环境将用于python程序安装与宿主系统隔离   
   
通过选择一个python解释器并创建一个./venv目录来创建一个新的虚拟环境，并激活该虚拟环境
```
virtualenv --system-site-packages -p python3 ./venv

.\venv\Scripts\activate
```
在虚拟环境中安装软件包，而不会影响主机系统设置，首先升级pip
```
pip install --upgrade pip

pip list  # show packages installed within the virtual environment
```
退出virtualenv 
```
deactivate  # don't exit until you're done using TensorFlow
```

3. 安装TensorFlow pip包
以下为可供下载的TensorFlow包
- tensorflow         —Current release for CPU-only (recommended for beginners)
- tensorflow-gpu     —Current release with GPU support (Ubuntu and Windows)
- tf-nightly         —Nightly build for CPU-only (unstable)
- tf-nightly-gpu     —Nightly build with GPU support (unstable, Ubuntu and Windows)

虚拟机环境下安装TensorFlow（安装时间稍长，耐心等待）
```
pip install --upgrade tensorflow
```
验证安装结果
```
python -c "import tensorflow as tf; print(tf.__version__)"
```
输出结果
```
1.11.0
```
#### 安装结束
*****
